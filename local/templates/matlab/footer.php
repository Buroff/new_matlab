
                        	

            <? if($APPLICATION->GetCurPage(false) === '/test/test-contacts.php'):?>
                    <!-- contacts page -->
                                </div>

                            </div>
                        </div>

                    </section>
                    <!-- / contacts page -->
            <? endif;?>


            <footer>
                <div class="grid-container">
                    <div class="grid-x grid-padding-x">
                        <div class="xlarge-3 cell show_for_xlarge">
                            <h4>Продукты</h4>
                            <ul>
                                <li><a href="#">Раздел</a></li>
                                <li><a href="#">Раздел</a></li>
                                <li><a href="#">Раздел</a></li>
                                <li><a href="#">Раздел</a></li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell show_for_xlarge">
                            <h4>Услуги</h4>
                            <ul>
                                <li><a href="#">Раздел</a></li>
                                <li><a href="#">Раздел</a></li>
                                <li><a href="#">Раздел</a></li>
                                <li><a href="#">Раздел</a></li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell">
                            <h4>Контакты</h4>
                            <ul class="footer_contacts">
                                <li>matlab@sl-matlab.ru</li>
                                <li><span>115088 г. Москва, </span>
                                    <span>2-й Южнопортовый проезд,</span> д. 31, стр. 4
                                </li>
                                <li>+7 (495) 009 65 85</li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell">
                            <h4 class="show_for_xlarge">Подписка</h4>
                            <ul class="podpiska">
                                <li class="show_for_xlarge">Подпишитесь на новости</li>
                                <li class="show_for_xlarge">
                                    <form action="">
                                        <div class="input-group">
                                            <input class="input-group-field" type="email" placeholder="Enter your e-mail here">
                                            <div class="input-group-button">
                                                <button type="submit" class="button">
                                                    <i class="ico-telegram"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </li>
                                <li>
                                    <ul class="footer_social_links">
                                        <li><a href="#"><i class="facebook"></i></a></li>
                                        <li><a href="#"><i class="twitter"></i></a></li>
                                        <li><a href="#"><i class="rss"></i></a></li>
                                        <li><a href="#"><i class="insta"></i></a></li>
                                        <li><a href="#"><i class="google"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="large-12 cell">
                            <p class="footer_copiright">© г.2018 Рязань</p>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="dropdown-pane" id="question" data-dropdown data-auto-focus="true">
                <div class="question_form_wrap">
                    <button class="close-button" data-close="">×</button>
                    <h3>Задать вопрос</h3>
                    <form>
                        <div class="grid-x grid-margin-x">
                            <div class="cell medium-6">
                                <input type="text" placeholder="E-mail">
                            </div>
                            <div class="cell medium-6">
                                <input type="text" placeholder="Имя">
                            </div>
                            <div class="cell medium-12">
                                <label class="radio">
                                    <input name="radio" type="radio" />
                                    <div class="radio__text">Задать вопрос</div>
                                </label>
                                <label class="radio">
                                    <input name="radio" type="radio" />
                                    <div class="radio__text">Услуги</div>
                                </label>
                                <label class="radio">
                                    <input name="radio" type="radio" />
                                    <div class="radio__text">Пилотный проект</div>
                                </label>
                                <label class="radio">
                                    <input name="radio" type="radio" />
                                    <div class="radio__text">Обучение</div>
                                </label>
                                <label class="radio">
                                    <input name="radio" type="radio" />
                                    <div class="radio__text">Лицензии</div>
                                </label>
                            </div>
                            <div class="cell medium-12">
                                <textarea name="" id="" rows="10">Текст сообщения</textarea>
                            </div>
                            <div class="cell medium-12 xlarge-8">
                            </div>
                            <div class="cell medium-12 xlarge-4 question_form_button_wrap">
                                <button type="submit">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="dropdown-pane" id="profile_settings" data-position="bottom" data-alignment="center" data-dropdown data-auto-focus="true">
                <div class="profile_settings_wrap">
                    <ul class="profile_settings_links">
                        <li>
                            <a href="#">
            <i class="ico_settings"></i>
            <span>Настройки</span>
        </a>
                        </li>
                        <li>
                            <a href="#">
          <i class="ico_exit"></i>
            <span>Выход</span>
      </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/main.min.js"></script>
    <script>
    $(document).ready(function() {
        var offCanvas = new Foundation.OffCanvas($('#offCanvas-map'));
    });
    </script>
</body>

</html>