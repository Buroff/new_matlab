<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- Useful meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="index, follow, noodp">
    <meta name="googlebot" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">

   
    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/css/style.min.css" />
	<?$APPLICATION->ShowHead();?>

	<title><?$APPLICATION->ShowTitle()?></title>

</head>

<body>


    <div class="off-canvas-wrapper">
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>

        <div class="off-canvas position-left" id="mobile_menu" data-off-canvas data-transition="overlap">
        	<div class="mobile_menu_wrap">
        		<div class="mobile_menu_header">
        			<button class="close-button" data-close="">×</button>
        			<a href="#" class="mobile__logo"></a>
        		</div>
        		<div class="mobile_menu_body">
        			<div class="mobile_menu_title">Меню</div>
        			<ul>
        				<li>
        					<a href="#">Продукты и услуги</a>
        				</li>
        				<li>
        					<a href="#">Мероприятия</a>
        				</li>
        				<li>
        					<a href="#">Публикации</a>
        				</li>
        				<li>
        					<a href="#">Обучение</a>
        				</li>
        				<li>
        					<a href="#">ВУЗы</a>
        				</li>
        				<li>
        					<a href="#">Сообщество</a>
        				</li>
        				<li>
        					<a href="#">Документация</a>
        				</li>
        				<li>
        					<a href="#">Контакты</a>
        				</li>
        			</ul>
        		</div>
        		<div class="mobile_menu_footer"></div>
        	</div>
        </div>


        <div class="off-canvas-content" data-off-canvas-content>
            <div class="top_bar">
                <div class="grid-container show_for_xlarge">
                    <div class="grid-x grid-padding-x">
                        <div class="xlarge-6 cell">
                            <ul class="menu question_button">
                                <li>
                                    <button class="button" type="button" data-toggle="question">Задать вопрос</button>
                                </li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell">
                            <ul class="menu align-right top_bar_contacts">
                                <li>+7 (495) 009 65 85</li>
                                <li>matlab@sl-matlab.ru</li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell">
                            <ul class="menu align-right cabinet">
                                <li><a href="#" class="ico_mail">
                            <span class="mail_numbers">1</span>
                        </a></li>
                                <li>
                                    <button class="button" type="button" data-toggle="profile_settings">Вход/Регистрация</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="grid-container hidden_for_xlarge">
                    <div class="grid-x grid-padding-x">
                        <div class="small-6 cell">
                            <ul class="menu question_button">
                                <li class="mobile__logo_wrap">
                                    <a href="#" class="mobile__logo"></a>
                                </li>
                                <li>
                                    <button class="button" type="button" data-toggle="question">Задать вопрос</button>
                                </li>
                            </ul>
                        </div>
                        <div class="small-6 cell">
                            <ul class="menu align-right cabinet">
                                <li>
                                     <button class="user_button" type="button" data-toggle="profile_settings">
                                    Профиль
                                    <span class="ico_user"></span>
                                    </button>
                                </li>
                                <li>
                                    <button class="menu_button" type="button" data-toggle="mobile_menu">Меню 
                                        <span class="ico_menu"></span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <header class="contact_header">
            	
            	<div class="grid-container">
                    <div class="grid-x grid-padding-x">


                    	<div class="large-12 cell">
                    		<!-- <ul class="menu desctop_menu">
                                                            <li><a href="#" class="desctop_menu_logo"></a></li>
                                                              <li><a href="#">Продукты и услуги</a></li>
                                                              <li><a href="#">Мероприятия</a></li>
                                                              <li><a href="#">Публикации</a></li>
                                                              <li><a href="#">Обучение</a></li>
                                                              <li><a href="#">ВУЗы</a></li>
                                                              <li><a href="#">Сообщество</a></li>
                                                              <li><a href="#">Документация</a></li>
                                                              <li><a href="#">Контакты</a></li>
                                                              <li><a href="#" class="ico_search">Four</a></li>
                                                        </ul> -->

                            <?
            $APPLICATION->IncludeComponent("bitrix:menu", "matlab_top", array(
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "leftfirst",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>
                    	</div>


                        <div class="large-12 cell">
							<!--  <ul class="breadcrumbs">
							    <li><a href="#">Matlkab</a></li>
							    <li>
							       Контакты
							    </li>
							</ul> -->


							<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "matlab", array(
									"START_FROM" => "0",
									"PATH" => "",
									"SITE_ID" => "-"
								),
								false,
								Array('HIDE_ICONS' => 'Y')
							);?>
							
						</div>

						<div class="large-12 cell">
							<h1 style="text-transform: uppercase;"><?=$APPLICATION->ShowTitle(false);?></h1>
						</div>

						<div class="medium-11 large-8 xlarge-6 cell">

							 <?if($APPLICATION->GetCurPage(false) === '/test/test-contacts.php'):?>
								 <?$APPLICATION->IncludeComponent(
									"bitrix:main.include", 
									".default", 
									array(
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "inc",
										"EDIT_TEMPLATE" => "",
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => "/include/contacts.php"
									),
									false
								);?>
							<?endif;?>

                        </div>

                    </div>
                </div>


            </header>

            

            <? 
echo $APPLICATION->GetCurPage(false);
            if($APPLICATION->GetCurPage(false) === '/test/test-contacts.php' || $APPLICATION->GetCurPage(false) === '/contacts/'):?>
                <!-- contacts page -->
                        <section class="contact_content">

                        	<div class="grid-container">
                                <div class="grid-x grid-padding-x">
                                    
                                    <div class="large-12 cell">

                <!-- / contacts page -->
            <? endif;?>

                        