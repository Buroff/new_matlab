<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- Useful meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="index, follow, noodp">
    <meta name="googlebot" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">
 <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/style.min.css"> 
        <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>
    <div class="off-canvas-wrapper">
        <div id="panel"><?$APPLICATION->ShowPanel();?></div>
        
             <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:menu", 
                        "matlab_top_mobile", 
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "matlab_top"
                        ),
                        false
                    );
                    ?>


       

        <div class="off-canvas-content" data-off-canvas-content>
            <div class="top_bar">
                <div class="grid-container show_for_xlarge">
                    <div class="grid-x grid-padding-x">
                        <div class="xlarge-6 cell">
                            <ul class="menu question_button">
                                <li>
                                    <button class="button" type="button" data-toggle="question">Задать вопрос</button>
                                </li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell">
                            <ul class="menu align-right top_bar_contacts">
                                <li>+7 (495) 009 65 85</li>
                                <li>matlab@sl-matlab.ru</li>
                            </ul>
                        </div>
                        <div class="xlarge-3 cell">
                            <ul class="menu align-right cabinet">
                                <li><a href="#" class="ico_mail">
                            <span class="mail_numbers">1</span>
                        </a></li>
                                <li>
                                    <button class="button" type="button" data-toggle="profile_settings">Вход/Регистрация</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="grid-container hidden_for_xlarge">
                    <div class="grid-x grid-padding-x">
                        <div class="small-6 cell">
                            <ul class="menu question_button">
                                <li class="mobile__logo_wrap">
                                    <a href="#" class="mobile__logo"></a>
                                </li>
                                <li>
                                    <button class="button" type="button" data-toggle="question">Задать вопрос</button>
                                </li>
                            </ul>
                        </div>
                        <div class="small-6 cell">
                            <ul class="menu align-right cabinet">
                                <li>
                                    <button class="user_button" type="button" data-toggle="profile_settings">
                                        Профиль
                                        <span class="ico_user"></span>
                                    </button>
                                </li>
                                <li>
                                    <button class="menu_button" type="button" data-toggle="mobile_menu">Меню
                                        <span class="ico_menu"></span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <header class="inner_header">
                <div class="grid-container">
                    <div class="grid-x grid-padding-x">
                        <div class="large-12 cell desctop_menu_wrap">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:menu", 
                        "matlab_top", 
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "matlab_top"
                        ),
                        false
                    );
                    ?>
                           
                        </div>
                        <div class="large-12 cell">
                         <!--    <ul class="breadcrumbs">
                                <li><a href="#">Matlkab</a></li>
                                <li>
                                    Новости
                                </li>
                            </ul> -->

                            
                            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "matlab", array(
                                    "START_FROM" => "0",
                                    "PATH" => "",
                                    "SITE_ID" => "-"
                                ),
                                false,
                                Array('HIDE_ICONS' => 'Y')
                            );?>


                        </div>
                        <div class="large-12 cell"><!-- 
                                <h1>НОВОСТИ И СТАТЬИ</h1> -->
                                 <h1><?$APPLICATION->ShowTitle()?></h1> 
                        </div>
                        <div class="medium-11 large-8 xlarge-6 cell">
                                    <p>Друзья. Мир меняется и Matlab вместе с ним. 
                                    Мы изучали отзывы и решили обновить сайт. 
                                    Специально для вас переаботали структуру разделов 
                                    и самое важное вынесли на главную.</p>
                        </div>
                    </div>
                </div>
            </header>

<!-- WORK AREA -->


            