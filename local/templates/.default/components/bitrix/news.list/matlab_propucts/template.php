<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// dump($arParams["SET_META_KEYWORDS"]);
//dump($templateName);
//dump($templateFile);
/*$path = $component->GetPath(); // через объект компонента получаем путь к папке компонента относительно корня сайта
$name = $this->GetName(); // через объект шаблона получаем имя шаблона компонента

dump($path);
echo '<br/>';
dump($name);
*/
?>
<div class="index__products">

<div class="grid-container">

	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>	

	<div class="grid-x grid-padding-x">

	 		<div class="large-12 cell">
                    <h2>Продукты и услуги</h2>
            </div> 
	
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>

				 <div class="small-6 medium-3 xlarge-2 cell" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <a href="#" class="index__products_item">
			                    <i class="ico-brain"></i>
			                    <span>Глубокое</span><span>обучение</span>
			                </a>
                </div>

			<?endforeach;?>

	</div>

	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>	

</div>

	


</div>


