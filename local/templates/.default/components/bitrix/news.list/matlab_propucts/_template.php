<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// dump($arParams["SET_META_KEYWORDS"]);
//dump($templateName);
//dump($templateFile);
/*$path = $component->GetPath(); // через объект компонента получаем путь к папке компонента относительно корня сайта
$name = $this->GetName(); // через объект шаблона получаем имя шаблона компонента

dump($path);
echo '<br/>';
dump($name);
*/
?>

<div class="news-box">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>	
	<div class="title">
		<h2>
			<a href="<?="/".ltrim(str_replace("#SITE_DIR#", SITE_DIR, $arResult['LIST_PAGE_URL']), "/")?>"><?=$arResult["NAME"]?></a>
		</h2>
	</div>
	<div class="cnt">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="preview">
				<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
					<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				
						<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="news" title="news"/>
					<?else:?>
					
						<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="news" title="news"/>
					<?endif;?>
				<?endif?>
			</div>
			<div class="n-staff">
				<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
					<span class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
				<?endif?>

				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
					<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
						<h3><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h3>
					<?else:?>
						<h3><?=$arItem["NAME"]?></h3>
					<?endif;?>
				<?endif;?>

				<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
					<p class="txt">
						<?=$arItem["PREVIEW_TEXT"];?>
					</p>									
				<?endif;?>

				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
					<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
						<div class="more-btn-box">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="link"><?=GetMessage('DETAIL')?></a>
						</div>					
					<?endif;?>
				<?endif;?>

			</div>
		</div>
	<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>		
	</div>
</div>

