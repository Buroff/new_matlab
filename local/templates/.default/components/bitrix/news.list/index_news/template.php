<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

 <?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?//=$arResult["NAV_STRING"]?><br />
<?endif;?>
 <section class="index_news">
 	<div class="grid-container">
 		<div class="grid-x grid-padding-x">
 			
 			 <div class="large-12 cell">
                            <h2>Новости</h2>

                           
             </div>

             <div class="large-12 cell">
             	
             	<div class="grid">
             		
             		<div class="grid-sizer"></div>
             		<?foreach($arResult["ITEMS"] as $arItem):?>
             			<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		             		<div class="grid-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

								<?
								$type = '';

									switch ($arItem["category"]) {
								    
								    case 'success_story':
								        $type =  "item_history";
								        $title = 'история успеха';
								        break;

								    case 'news':
								        $type =  "item_news";
								        $title = 'новость';
								        break;

								    case 'post':
								        $type =  "item_post";
								        $title = 'блог';
								        break;

								    case 'video_title':
								        $type =  "post_video_title";
								        $title = 'видео';
								        break;

								    case 'video_fon':
								        $type =  "post_video_fon";
								        $title = 'видео';
								        break;


								    case 'post_project':
								        $type =  "post_project";
								        $title = 'проект';
								        break;
								    
								    default:
								       $type =  "item_news";
								       $title = 'новость';
								       break;
								}
?>

		                                    <div class="blog_post_list_item <?echo $type?>">
		                                        <div class="blog_post_list_item_header">
		                                            <span class="blog_post_list_item_date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		                                            <span class="blog_post_list_item_category"><?=$title?></span>
		                                        </div>
		                                        <div class="blog_post_list_item_title">
			                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
			                                                <!-- <span>Параллельные вычисления в</span>
			                                            <span>ежедневных инженерных</span>
			                                            <span>задачах</span> -->
			                                           <span> <?echo $arItem["NAME"]?></span>
			                                           </a>
		                                        </div>


		                                        <div class="blog_post_list_item_footer">
		                                           <!--  <span>Роман Мнев</span> -->
		                                            <?if($arItem["refs_speaker"]):?>	

		                                             <? 
		                                             // pr($arItem["refs_speaker"]);    ?>	                                    

				                                            <?
				                                            $APPLICATION->IncludeComponent(
															"bitrix:news.detail",
															"speaker_news_detail",
															Array(
																"ACTIVE_DATE_FORMAT" => "d.m.Y",
																"ADD_ELEMENT_CHAIN" => "N",
																"ADD_SECTIONS_CHAIN" => "Y",
																"AJAX_MODE" => "N",
																"AJAX_OPTION_ADDITIONAL" => "",
																"AJAX_OPTION_HISTORY" => "N",
																"AJAX_OPTION_JUMP" => "N",
																"AJAX_OPTION_STYLE" => "Y",
																"BROWSER_TITLE" => "-",
																"CACHE_GROUPS" => "Y",
																"CACHE_TIME" => "36000000",
																"CACHE_TYPE" => "A",
																"CHECK_DATES" => "Y",
																"DETAIL_URL" => "",
																"DISPLAY_BOTTOM_PAGER" => "Y",
																"DISPLAY_DATE" => "Y",
																"DISPLAY_NAME" => "Y",
																"DISPLAY_PICTURE" => "Y",
																"DISPLAY_PREVIEW_TEXT" => "Y",
																"DISPLAY_TOP_PAGER" => "N",
																"ELEMENT_CODE" => "",
																// "ELEMENT_ID" => "1156",
																"ELEMENT_ID" => $arItem["refs_speaker"],
																"FIELD_CODE" => array("",""),
																"IBLOCK_ID" => "22",
																"IBLOCK_TYPE" => "Event",
																"IBLOCK_URL" => "",
																"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
																"MESSAGE_404" => "",
																"META_DESCRIPTION" => "-",
																"META_KEYWORDS" => "-",
																"PAGER_BASE_LINK_ENABLE" => "N",
																"PAGER_SHOW_ALL" => "N",
																"PAGER_TEMPLATE" => ".default",
																"PAGER_TITLE" => "Страница",
																"PROPERTY_CODE" => array("",""),
																"SET_BROWSER_TITLE" => "Y",
																"SET_CANONICAL_URL" => "N",
																"SET_LAST_MODIFIED" => "N",
																"SET_META_DESCRIPTION" => "Y",
																"SET_META_KEYWORDS" => "Y",
																"SET_STATUS_404" => "N",
																"SET_TITLE" => "Y",
																"SHOW_404" => "N",
																"STRICT_SECTION_CHECK" => "N",
																"USE_PERMISSIONS" => "N",
																"USE_SHARE" => "N"
															)
														);
														?>
													<?endif;?>
		                                        </div>


		                                        <span class="ico_book"></span>
		                                    </div>
		                    </div>
                    <?endforeach;?>

                    <div class="grid-item">
 <a href="/news/" class="news_button_more">Смотреть все новости</a><br>

				</div>

             	</div>
             </div>
 		</div>
 		
   </div>
</section>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?//=$arResult["NAV_STRING"]?>
<?endif;?>


<