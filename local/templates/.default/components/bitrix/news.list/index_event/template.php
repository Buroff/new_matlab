<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index_events">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<?if($arParams["DISPLAY_TOP_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?><br />
			<?endif;?>

			 <div class="large-12 cell">
                            <h2>Мероприятия</h2>
             </div>


			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="small-12 medium-6 large-6 xlarge-4 cell" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="index__event__item">
							

							    <div class="index__event__item_cont">
                                    <div class="event__item_date">
                                        <span class="event_date"><?
                                        // echo date("d.m.Y", strtotime($arItem["DISPLAY_ACTIVE_FROM"]))
                                        echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
                                        <!-- <span class="event_week_day">Суббота</span> -->
                                    </div>
                                    <div class="event__item_title">
                                        <a href="#">
                                        	<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                                        	<span><?echo $arItem["NAME"]?></span>
                                        	</a>
			                               <!--  <span>Параллельные вычисления в</span>
			                               <span>ежедневных инженерных задачах</span> -->

			                            </a>
                                    </div>

                                    <div class="event__item_author">
                                        <div class="date">
                                            <div>
                                                <span class="event_city">москва</span>
                                                <span class="event_time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
                                            </div>
                                            <div>
                                                <a href="#" data-crm="<?echo $arItem["ID_CRM"]?>" class="event_link" data-open="event-registration">Записаться на вебинар</a>
                                            </div>
                                        </div>

										<?if($arItem["refs_speaker"]):?>		                                        <?$APPLICATION->IncludeComponent(
													"bitrix:news.detail",
													"speaker",
													Array(
														"ACTIVE_DATE_FORMAT" => "d.m.Y",
														"ADD_ELEMENT_CHAIN" => "N",
														"ADD_SECTIONS_CHAIN" => "Y",
														"AJAX_MODE" => "N",
														"AJAX_OPTION_ADDITIONAL" => "",
														"AJAX_OPTION_HISTORY" => "N",
														"AJAX_OPTION_JUMP" => "N",
														"AJAX_OPTION_STYLE" => "Y",
														"BROWSER_TITLE" => "-",
														"CACHE_GROUPS" => "Y",
														"CACHE_TIME" => "36000000",
														"CACHE_TYPE" => "A",
														"CHECK_DATES" => "Y",
														"DETAIL_URL" => "",
														"DISPLAY_BOTTOM_PAGER" => "Y",
														"DISPLAY_DATE" => "Y",
														"DISPLAY_NAME" => "Y",
														"DISPLAY_PICTURE" => "Y",
														"DISPLAY_PREVIEW_TEXT" => "Y",
														"DISPLAY_TOP_PAGER" => "N",
														"ELEMENT_CODE" => "",
														// "ELEMENT_ID" => "1156",
														"ELEMENT_ID" => $arItem["refs_speaker"][0],
														"FIELD_CODE" => array("",""),
														"IBLOCK_ID" => "22",
														"IBLOCK_TYPE" => "Event",
														"IBLOCK_URL" => "",
														"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
														"MESSAGE_404" => "",
														"META_DESCRIPTION" => "-",
														"META_KEYWORDS" => "-",
														"PAGER_BASE_LINK_ENABLE" => "N",
														"PAGER_SHOW_ALL" => "N",
														"PAGER_TEMPLATE" => ".default",
														"PAGER_TITLE" => "Страница",
														"PROPERTY_CODE" => array("",""),
														"SET_BROWSER_TITLE" => "Y",
														"SET_CANONICAL_URL" => "N",
														"SET_LAST_MODIFIED" => "N",
														"SET_META_DESCRIPTION" => "Y",
														"SET_META_KEYWORDS" => "Y",
														"SET_STATUS_404" => "N",
														"SET_TITLE" => "Y",
														"SHOW_404" => "N",
														"STRICT_SECTION_CHECK" => "N",
														"USE_PERMISSIONS" => "N",
														"USE_SHARE" => "N"
													)
												);?>
										<?endif;?>

                                        

                                    </div>
                                </div>


                            <div class="index__event__item_button">
                                <a href="#" class="vebinar">
                                    <span>Вебинар</span>
                                </a>
                            </div>
						</div>
				</div>
			<?endforeach;?>
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<br /><?=$arResult["NAV_STRING"]?>
			<?endif;?>
		</div>
	</div>
</div>

<!-- окно личного кабинета -->

<div class="reveal" id="event-registration" data-reveal>
    <div class="form_wrap">
        <button class="close-button" data-close="">×</button>
        <form>
            <iframe src="#" width="100%" height="700px" frameBorder="0" scrolling="no"
                    style="overflow: hidden" id="frame_event"></iframe>

        </form>

    </div>
</div>
