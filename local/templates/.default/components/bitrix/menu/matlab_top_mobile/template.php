<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


 <div class="off-canvas position-left" id="mobile_menu" data-off-canvas data-transition="overlap">
		<div class="mobile_menu_wrap">
                <div class="mobile_menu_header">
                    <button class="close-button" data-close="">×</button>
                    <a href="#" class="mobile__logo"></a>
                </div>
                <div class="mobile_menu_body">
                    <div class="mobile_menu_title">Меню</div>
                    <?if (!empty($arResult)):?>
						<ul>
							<?foreach($arResult as $arItem):?>
								<?if($arItem["SELECTED"]):?>
									<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
								<?else:?>
									<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
								<?endif?>
							<?endforeach?>
						</ul>      
					<?endif?>             
            
                </div>
                <div class="mobile_menu_footer"></div>
            </div> 
 </div>

