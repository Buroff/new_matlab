<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
  <section class="news_content">
              <div class="grid-container">
                    <div class="grid-x grid-padding-x">
                        <div class="large-12 cell date_block">
                           <!--  24 сентября/  2018г. -->
                            <?echo $arItem["ACTIVE_FROM"]?>
                        </div>

                        <div class="large-12 cell prew_block">
                            <?echo $arResult["PREVIEW_TEXT"];?>
                        </div>

                        <div class="large-8 cell large-offset-2">
                            
                            <div class="text_block">
                                <h2><?=$arResult["NAME"]?></h2>
                                <?echo $arResult["DETAIL_TEXT"];?>
                            </div>

                            <div class="grid-x grid-padding-x">
                                <div class="large-10 cell">
                                    <div class="rectangle">
                                        <h4><?=$arResult["NAME"]?></h4>
                                    <ol>
                                        <li>обработка больших объёмов экспериментальных данных;</li>
                                        <li>получение характеристик сложных объектов;</li>
                                        <li>построение точной модели по экспериментальным данным;</li>
                                    </ol>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="text_block">
                               <?=$arResult["NAME"]?>
                            </div>

              <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
                <div class="img_wrap">
                  <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                  width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
                  height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
                  alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                  title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
                  />
                </div>
              <?endif?>
            


                            <div class="text_block">
                                <div class="grid-x grid-padding-x">
                                <div class="large-6 cell">
                                   <h3><?=$arResult["NAME"]?></h3> 
                                   <?echo $arResult["PREVIEW_TEXT"];?>
                                </div>
                                <div class="large-6 cell">
                                    <h3><?=$arResult["NAME"]?></h3>
                                    <?echo $arResult["PREVIEW_TEXT"];?>
                                </div>
                            </div>
                            </div>

                            <div class="text_block">
                              <?echo $arResult["DETAIL_TEXT"];?>
                            </div>
                            
                            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
                <div class="img_wrap">
                  <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                  width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
                  height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
                  alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                  title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
                  />
                </div>
              <?endif?>

                           <!--  <div class="text_block">
                               <h4>Небольшие особенности корпуса</h4>
                               <ul>
                                   <li>поверхностные царапины на крыжке аккумулятора</li>
                                   <li>задир на крыжке возле правого угла</li>
                                   <li>потертости на логотипе</li>
                               </ul>
                           </div>
                           <div class="text_block">
                               <p>Каждый компонент MacBook был тщательно спроектирован с учётом ограниченного пространства его невероятно тонкого и лёгкого корпуса. </p>
                           </div>
                            -->

                            <div class="text_block">
                               <ul class="accordion" id="accordion" data-accordion data-allow-all-closed="true">
                                  <li class="accordion-item" data-accordion-item>
                                    <a href="#" class="accordion-title">Скачать дополнительные  материалы</a>

                                    <div class="accordion-content" data-tab-content>
                                      <a href="#">
                                        <span class="ico_dop_file2">
                                        </span>
                                        <span>Скачать материалы</span>
                                      </a>
                                    </div>
                                  </li>
                                </ul>
                            </div>

                            <div class="tags_block">
                               <!--  <h4>Связанные теги</h4>
                               <a href="#">#matlab</a>
                               <a href="#">#обучение</a>
                               <a href="#">#трейнинги</a>
                               <a href="#">#семинар</a>
                               <a href="#">#вебинар</a>
                               <a href="#">#задачи</a>
                               <a href="#">#инженерениг</a>
                               <a href="#">#трейнинги</a>
                               <a href="#">#трейнинги</a>
                               <a href="#">#трейнинги</a>
                               <a href="#">#трейнинги</a>
                               <a href="#">#трейнинги</a> -->

                                <!-- здесь будут теги -->

                            </div>
                            <div class="social_block">
                                <!-- сюда вставить сервис с социальными кнопками -->
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </section>
