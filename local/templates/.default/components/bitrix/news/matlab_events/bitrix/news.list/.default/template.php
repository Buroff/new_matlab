<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="page_news">
                <div class="grid-container">
                   

                   <!-- FILTER -->
                    <div class="grid-x grid-padding-x">
                        <div class="large-10 cell">
                            <div class="event_filter_title">
                                Фильтрация
                            </div>
                        </div>
                        <div class="large-2 cell">
                            <div class="event_filter_title">
                                Сортировка
                            </div>
                        </div>
                    </div>

                    <div class="grid-x grid-padding-x">
                        <div class="large-10 cell">
                                <ul class="event_filter_body_list">
                                    <li>
                                       <button class="form_dropdown_button" type="button" data-toggle="form_dropdown3">Сферы применения</button>
                                        <div class="dropdown-pane bottom" id="form_dropdown3" data-dropdown>
                                            <label class="checkbox_container">Анализ данных
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Беспроводные системы связи
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Встраиваемые системы
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Вычислительная биология
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div> 
                                    </li>
                                    <li>
                                        <button class="form_dropdown_button" type="button" data-toggle="form_dropdown4">Индустрия</button>
                                        <div class="dropdown-pane bottom" id="form_dropdown4" data-dropdown>
                                            <label class="checkbox_container">Анализ данных
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Беспроводные системы связи
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Встраиваемые системы
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Вычислительная биология
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <button class="form_dropdown_button" type="button" data-toggle="form_dropdown5">Рабочие процессы</button>
                                        <div class="dropdown-pane bottom" id="form_dropdown5" data-dropdown>
                                            <label class="checkbox_container">Анализ данных
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Беспроводные системы связи
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Встраиваемые системы
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Вычислительная биология
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <button class="form_dropdown_button" type="button" data-toggle="form_dropdown6">Рабочие процессы</button>
                                        <div class="dropdown-pane bottom" id="form_dropdown6" data-dropdown>
                                            <label class="checkbox_container">Семинары
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Вебинары
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Тренинги
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="checkbox_container">Экзамены
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="checkbox_container">Архивные записи
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                </ul>
                        </div>
                        <div class="large-2 cell">
                            <ul class="event_filter_body_list second_list">
                                    <li>
                                       <button class="form_dropdown_button" type="button" data-toggle="form_dropdown2">По дате</button>
                                        <div class="dropdown-pane bottom" id="form_dropdown2" data-dropdown>
                                            <a href="#">от 1 до 31</a>
                                            <a href="#">от 31 до 1</a>
                                            <a href="#">сначала дешевые</a>
                                            <a href="#">сначала дорогие</a>
                                        </div> 
                                    </li>
                            </ul>
                        </div>
                    </div>

                    <!-- / FILTER -->


                    <div class="grid-x grid-padding-x">

                        <?foreach($arResult["ITEMS"] as $arItem):?>

                        <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>

                                <? 
                                $type = '';

                                switch ($arItem["category"]) {
                                    
                                    case 'success_story':
                                        $type =  "item_history";
                                        $title = 'история успеха';
                                        break;

                                    case 'news':
                                        $type =  "item_news";
                                        $title = 'новость';
                                        break;

                                    case 'post':
                                        $type =  "item_post";
                                        $title = 'блог';
                                        break;

                                    case 'video_title':
                                        $type =  "post_video_title";
                                        $title = 'видео';
                                        break;

                                    case 'video_fon':
                                        $type =  "post_video_fon";
                                        $title = 'видео';
                                        break;


                                    case 'post_project':
                                        $type =  "post_project";
                                        $title = 'проект';
                                        break;
                                    
                                    default:
                                       $type =  "item_news";
                                       $title = 'новость';
                                       break;
                                }
?>

                                <div class="small-12 medium-6 large-6 xlarge-4 cell" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                    <div class="index__event__item">
                                        <div class="index__event__item_cont">
                                            <div class="event__item_date">
                                                <!-- <span class="event_date">11 ИЮНЯ</span>
                                                <span class="event_week_day">Суббота</span> -->
                                                <span class="event_date" style="font-size: 13px; text-transform: capitalize;"><?echo $arItem["NAME"]?></span>

                                            </div>
                                            <div class="event__item_title">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                       <!--  <span>Параллельные вычисления в</span>
                                                        <span>ежедневных инженерных задачах</span> -->
                                                        <span><?echo $arItem["NAME"]?></span>
                                                </a>
                                            </div>
                                            <div class="event__item_author">
                                                <div class="date">
                                                    <div>
                                                        <span class="event_city">москва</span>
                                                        <span class="event_time">13:00</span>
                                                    </div>
                                                    <div>
                                                       <a href="#" data-crm="<?echo $arItem["ID_CRM"]?>" class="event_link" data-open="event-registration">Записаться на семинар</a>

                                                    </div>
                                                </div>

                                                <!-- <div class="event__item_author_name">
                                                    <div class="author_name">
                                                        Роман Мнев
                                                    </div>
                                                    <div class="author_img">
                                                        <img src="assets/img/photo.png" alt="">
                                                        <img src="assets/img/photo.png" alt="">
                                                        <img src="assets/img/photo.png" alt="">
                                                    </div>
                                                </div> -->

                                                            <?if($arItem["refs_speaker"]):?>
                                                                <?
                                                                $APPLICATION->IncludeComponent(
                                                                    "bitrix:news.detail",
                                                                    "speaker_event_detail",
                                                                    array(
                                                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                                        "ADD_ELEMENT_CHAIN" => "N",
                                                                        "ADD_SECTIONS_CHAIN" => "N",
                                                                        "AJAX_MODE" => "N",
                                                                        "AJAX_OPTION_ADDITIONAL" => "",
                                                                        "AJAX_OPTION_HISTORY" => "N",
                                                                        "AJAX_OPTION_JUMP" => "N",
                                                                        "AJAX_OPTION_STYLE" => "Y",
                                                                        "BROWSER_TITLE" => "-",
                                                                        "CACHE_GROUPS" => "Y",
                                                                        "CACHE_TIME" => "36000000",
                                                                        "CACHE_TYPE" => "A",
                                                                        "CHECK_DATES" => "Y",
                                                                        "DETAIL_URL" => "",
                                                                        "DISPLAY_BOTTOM_PAGER" => "Y",
                                                                        "DISPLAY_DATE" => "Y",
                                                                        "DISPLAY_NAME" => "Y",
                                                                        "DISPLAY_PICTURE" => "Y",
                                                                        "DISPLAY_PREVIEW_TEXT" => "Y",
                                                                        "DISPLAY_TOP_PAGER" => "N",
                                                                        "ELEMENT_CODE" => "",
                                                                        "ELEMENT_ID" => $arItem["refs_speaker"],
                                                                        "FIELD_CODE" => array(
                                                                            0 => "",
                                                                            1 => "",
                                                                        ),
                                                                        "IBLOCK_ID" => "22",
                                                                        "IBLOCK_TYPE" => "Event",
                                                                        "IBLOCK_URL" => "",
                                                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                                        "MESSAGE_404" => "",
                                                                        "META_DESCRIPTION" => "-",
                                                                        "META_KEYWORDS" => "-",
                                                                        "PAGER_BASE_LINK_ENABLE" => "N",
                                                                        "PAGER_SHOW_ALL" => "N",
                                                                        "PAGER_TEMPLATE" => ".default",
                                                                        "PAGER_TITLE" => "Страница",
                                                                        "PROPERTY_CODE" => array(
                                                                            0 => "",
                                                                            1 => "",
                                                                        ),
                                                                        "SET_BROWSER_TITLE" => "N",
                                                                        "SET_CANONICAL_URL" => "N",
                                                                        "SET_LAST_MODIFIED" => "N",
                                                                        "SET_META_DESCRIPTION" => "N",
                                                                        "SET_META_KEYWORDS" => "N",
                                                                        "SET_STATUS_404" => "N",
                                                                        "SET_TITLE" => "N",
                                                                        "SHOW_404" => "N",
                                                                        "STRICT_SECTION_CHECK" => "N",
                                                                        "USE_PERMISSIONS" => "N",
                                                                        "USE_SHARE" => "N",
                                                                        "COMPONENT_TEMPLATE" => "speaker_event_detail"
                                                                    ),
                                                                    false
                                                                );


                                                                ?>

                                                            <?endif;?>


                                            </div>
                                        </div>
                                        <div class="index__event__item_button">
                                                <a href="#" class="vebinar">
                                                    <span>Вебинар</span>
                                                </a>
                                        </div>
                                    </div>
                                </div>



                        <?endforeach;?>


                        <div class="small-12 cell">
                            <a href="#" class="page__event__item_button_more">Смотреть все события</a>
                        </div>


                    </div>
                    
    </div>
</div>

 <!-- окно личного кабинета -->

                                  <div class="reveal" id="event-registration" data-reveal>
                                    <div class="form_wrap">
                                    <button class="close-button" data-close="">×</button>
                                          <form>
                                            <iframe src="#" width="100%" height="700px" frameBorder="0" scrolling="no"
                                            style="overflow: hidden" id="frame_event"></iframe>
                                             
                                         </form>
                                       
                                    </div>
                                </div>

