<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail">
	<div class="event__item_author_name">
		<div class="author_name">
		     <?=$arResult["NAME"]?>
		</div>
		<div class="author_img">
		       <!--  <img src="assets/img/photo.png" alt="">
		        <img src="assets/img/photo.png" alt="">
		        <img src="assets/img/photo.png" alt=""> -->
		</div>
	</div>
</div>