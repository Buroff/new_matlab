<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>
<div class="index__products">
	<div class="grid-container">
				<div class="grid-x grid-padding-x">
					<?
					if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
					{
						$this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
						$this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

						?><div
							class="large-12 cell"
							id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>"
						><h2><?
							// echo (
							// 	isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
							// 	? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
							// 	: $arResult['SECTION']['NAME']
							// );
						?>Продукты и услуги</h2></div><?
					}
					if (0 < $arResult["SECTIONS_COUNT"])
					{
					?>


					<?			// var_dump($arResult["SECTIONS_COUNT"]);
					
								$i = 1;
								$j = 0;
					
								foreach ($arResult['SECTIONS'] as &$arSection)
								{
									$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
									$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

									// pr($arSection);

									switch ($arSection['CODE']) {
										case 'p3_0':
											 $picture = 'ico-network';
											break;

										case 'p2_6':
											 $picture = 'ico-finance';
												break;
										
										case 'p2_2':
											 $picture = 'ico-telecommaunication';
												break;

										case 'p2_1':
											 $picture = 'ico-control-system';
												break;


										case 'p2_2':
											 $picture = 'ico-telecommaunication';
												break;

										case 'p2_2':
											 $picture = 'ico-telecommaunication';
												break;


										case 'p2_2':
											 $picture = 'ico-telecommaunication';
												break;


										case 'p2_2':
											 $picture = 'ico-telecommaunication';
												break;


										default:
											$picture = 'ico-brain';
											break;
									}

									if (false === $arSection['PICTURE'])
										$arSection['PICTURE'] = array(
											'SRC' => $arCurView['EMPTY_IMG'],
											'ALT' => (
												'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
												? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
												: $arSection["NAME"]
											),
											'TITLE' => (
												'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
												? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
												: $arSection["NAME"]
											)
										);
									?>

									<div class="small-6 medium-3 xlarge-2 cell" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
									<a
										href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
										class="index__products_item"
										title="<? echo $arSection['PICTURE']['TITLE']; ?>"
									><i class="<?=$picture?>"></i><? echo $arSection['NAME']; ?></a>
									
									</div>
									<?

									$i++;

									$res = $arResult["SECTIONS_COUNT"] - $i;
									
									if($res==0 && $j == 0){
										
										?>
											<div class="small-12 medium-6 xlarge-4 cell">
										 		<a href="#" class="index__products_item ico-individual">
														Индивидуальная разработка</a>
											</div>

										<?

										$j++;

									}


								}
								unset($arSection);



		
					?>
					<!--<div class="small-12 medium-6 xlarge-4 cell">
				 		<a href="#" class="index__products_item ico-individual">
								Индивидуальная разработка</a>
					</div>-->

					<?
						echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
					}
					?>
				</div>
			</div>
</div>