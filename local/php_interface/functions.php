<?php

/// получаем список элементов инфоблока
function getCIBlockElementList($PARAMS, $arSelect, $filterID = null, $arNavStartParams = null){  
    CModule::IncludeModule("iblock");

      $arFilter = Array("IBLOCK_ID"=>$PARAMS['block_id'], "ACTIVE"=>"Y","SECTION_ID"=>$PARAMS['section_id']); 
      if($filterID != null){ $arFilter["><ID"] = $filterID;} 
      if($arNavStartParams == null){ $arNavStartParams = Array("nPageSize"=>100);}     

      $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, $arNavStartParams, $arSelect);   
      $array_line_full = array();

      while($ar = $res->GetNext()) {
            $array_line_full[]  = $ar;
      }

      return  $array_line_full;
}

// $PARAMS = array('nElementID' => 156333, 'section_id' => 1107, 'block_id' => 1);
// // $arNavStartParams = Array("nPageSize"=>2,"nElementID" => 156333);
// $arNavStartParams = Array();
// $filterID = array(156071 ,156078);
// $arSelect = Array("ID", "NAME", "PROPERTY_PROPERTYNAME");
// $arDb = getCIBlockElementList($PARAMS, $arSelect, $filterID, $arNavStartParams);

// $PARAMS = array('nElementID' => 13, 'section_id' => 12, 'block_id' => 9);
// $arNavStartParams = Array("nPageSize"=>2,"nElementID" => 15);
// $arNavStartParams = Array();
// $filterID = array(156071 ,156078);
// $arSelect = Array("ID", "NAME", "PROPERTY_PROPERTYNAME");
// $arDb = getCIBlockElementList($PARAMS, $arSelect, $filterID, $arNavStartParams);
